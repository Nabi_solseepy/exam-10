const express = require('express');

const router = express.Router();

const createRoute = connection => {
    router.get('/id', (req,res) => {
       connection.query('SELECT * FROM `coments`', (error, results)=> {
           if (error){
               res.status(500).send({error: 'Database error'})
           }
            res.send(results);
           })
        });
    router.get('/:', (req,res)=>{
        connection.query('SELECT * FROM `coments` WHERE  id=?',
            req.params.id,(error, results) =>{
            if (error){
                res.status(500).send({error :'Database error'})
            } else {
                res.status(404).send({error: 'item not found'})
            }
            })
    });
    router.delete('/:id', (req,res) => {
        connection.query('DELETE FROM `coments` WHERE id = ?', req.params.id, (error) => {
            if (!error) {
                res.send('Deleted successfully')
            } else {
                res.status(404).send({message: 'Deletion is impossible'})
            }
        })
    });
    return router

};

module.exports = createRoute;