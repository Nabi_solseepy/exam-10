const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const nanoid = require('nanoid');
const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadsPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});
const upload = multer({storage});

const createRoute = connection => {
    router.get('/', (req,res) => {
        connection.query('SELECT * FROM `news`', (error, results) => {
            if (error){
                res.status(500).send({error: 'Database error'})
            }
            res.send(results);
        })
    });
    router.get('/id:', (req,res)=>{
        connection.query('SELECT * FROM `news` WHERE  id=?',
            req.params.id,(error, results) =>{
                if (error){
                    res.status(500).send({error :'Database error'})
                } else {
                    res.status(404).send({error: 'item not found'})
                }
                res.send({message: 'ok'})
            })

    });

    router.post('/', upload.single('image'), (req, res) => {
        const item = req.body;
        if (req.file) {
            item.image = req.file.filename
        }
        connection.query('INSERT INTO `news` ( `title`,  `content`, image) VALUES (?, ?, ?)',
            [item.title, item.content,  item.image],
            (error, results) => {
                if (error) {
                    res.status(500).send({error: 'Database error'})
                }
                res.send({message: 'OK'})
            }
        )
    });

    return router
};

module.exports = createRoute;