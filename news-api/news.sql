CREATE SCHEMA `news-api` DEFAULT CHARACTER SET utf8;

USE `news-api`;

CREATE TABLE `news` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `content` TEXT NOT NULL,
  `image` VARCHAR(150) NULL,
  `date` INT NULL,
  PRIMARY KEY (`id`));
  
  CREATE TABLE `coments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `news_id` INT NULL,
  `author` VARCHAR(255) NULL,
  `comment` TEXT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `news_id_fk_idx` (`news_id` ASC),
  CONSTRAINT `news_id_fk`
    FOREIGN KEY (`news_id`)
    REFERENCES `news-api`.`news` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE);
    
    
    
INSERT INTO `news`(`id`, `title`,`content`)
VALUES
(1, 'Lorem ipsum dplor sit amet', 'Proin venenaits mi orci quis');

INSERT INTO `coments`(`id`, `news_id`, `comment`)
VALUES
(1, 1, 'lorem haha') 