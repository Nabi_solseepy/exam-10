const express = require('express');
const cors = require('cors');
const mysql =require('mysql');
const app = express();
const coments = require('./app/coments');
const news = require('./app/news');

app.use(express.json());
app.use(express.static('public'));
app.use(cors());

const port = 8000;





const connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'user',
    password : '1qaz@WSX29',
    database :  'news-api'
});

app.use('/coments', coments(connection));
app.use('/news', news(connection));

connection.connect((err) =>  {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }

    console.log('connected as id ' + connection.threadId);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});
