import React, {Component} from 'react';
import {connect} from "react-redux";
import {postGet, postNews} from "../../store/action";
import {Card, CardBody, CardImg, CardTitle} from "reactstrap";

class Posts extends Component {
    componentDidMount() {
        this.props.postGet()
    }

    render() {
        return (
            <div className="container">
                <h1> Posts</h1>
                {this.props.news.map((news, id ) => {
                  return(
                      <Card key={id} onClick={() => this.props.postGet(news.id)}  >
                          <CardImg  />
                          <CardBody>
                              <CardTitle>{news.title}</CardTitle>
                              <CardTitle>{news.content}</CardTitle>

                          </CardBody>
                      </Card>
                  )
                })}
            </div>
        );
    }
}
const mapStateToProps = state =>({
    news: state.news
});

const mapDispachToProps = dispatch => ({
    postGet: () => dispatch(postGet()),
    postNews: (posts,history)=> dispatch(postNews(posts,history))

});
export default connect(mapStateToProps,mapDispachToProps)(Posts);