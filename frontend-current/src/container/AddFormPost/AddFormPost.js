import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {postGet, postNews} from "../../store/action";

class AddFormPost extends Component {
    state = {
        title: '',
        content: '',
        image: ''

    };

    componentDidMount() {
        console.log(this.props.history)
    }

    valuePost = (event) => {
        const  name = event.target.name;
        this.setState({[name]: event.target.value})
    };

    handlerPost = (event) => {
        event.preventDefault();

        const PostData = {...this.state};
        this.props.postNews(PostData, this.props.history)

    };
    render() {
        return (
            <Fragment>
                <h1>Add new post</h1>
       <div className="container">
           <Form className='pt-5'>

               <FormGroup mt={5} row>
                   <Label for="exampleEmail" sm={2}>Title</Label>
                   <Col sm={10}>
                       <Input type="name" onChange={this.valuePost}  values={this.props.title} name="name" id="exampleEmail"/>
                   </Col>
               </FormGroup>
               <FormGroup row>
                   <Label for="examplePassword" sm={2}>Content</Label>
                   <Col sm={10}>
                       <Input type="textarea" onChange={this.valuePost}    name="number" id="examplePassword"  />
                   </Col>
               </FormGroup>
               <FormGroup row>
                   <Label for="examplePassword" sm={2}>Image</Label>
                   <Col sm={10}>
                       <Input type="file" onChange={this.valuePost} />
                   </Col>
               </FormGroup>


               <Button color="danger" onClick={this.handlerPost }>save</Button>
           </Form>
       </div>

            </Fragment>

        );

    }
}

const mapStateToProps = state =>({
    news: state.news
});

const mapDispachToProps = dispatch => ({
    postGet: () => dispatch(postGet()),
    postNews: (posts, history)=> dispatch(postNews(posts, history))

});
export default connect(mapStateToProps, mapDispachToProps)(AddFormPost);