import React, {Component} from 'react';
import {Button, Navbar, NavbarBrand, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

class Header extends Component {
    render() {
        return (
            <Navbar color="secondary" light>
                <NavbarBrand href="/" className="mr-auto">News</NavbarBrand>
                <NavLink tag={RouterNavLink} exact to="/add/post"> <Button color="info">Add new post</Button></NavLink>
            </Navbar>
        );
    }
}

export default Header;