import React, {Component, Fragment} from 'react';
import './App.css';
import Header from "./component/Header";
import {Route} from "react-router";
import AddFormPost from "./container/AddFormPost/AddFormPost";
import Posts from "./container/PostsNews/Posts";

class App extends Component {
  render() {
    return (
     <Fragment>

         <Header/>
              <Route path="/" exact component={Posts}/>
             <Route path="/add/post" component={AddFormPost}/>



     </Fragment>
    );
  }
}

export default App;
