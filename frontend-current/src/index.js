import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import {createStore, applyMiddleware,  } from 'redux';
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import thunkMiddleware from 'redux-thunk'
import reducer from './store/reducer'





const store = createStore(
    reducer, applyMiddleware(thunkMiddleware)
);

const app = (
    <BrowserRouter>
        <Provider store={store}>
            <App/>
        </Provider>
    </BrowserRouter>
);

ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
